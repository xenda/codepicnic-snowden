
import sublime
import sublime_plugin
import time
import os.path
import sys

PY2 = sys.version_info[0] == 2

if PY2:
	import urllib
	import urlparse as urllibparse
else:
	import urllib.parse as urllibparse

import threading
import hashlib
import http.client
import base64

global code_picnic_emitter, settings, activity_json_template
global fileHash, lastRow, lastCol, last_viewport_x, last_viewport_y

fileHash = ""
settings = sublime.load_settings('CodePicnicEmitter.sublime-settings')
# SECRET 

# t = time_gmt_ms
# r = row
# c = column
# x = viewport_x
# y = viewport_y
# d = code
# f = file
# g = github username

activity_json_template = '{"t":"%s","r":"%s","c":"%s","x":"%s","y":"%s","d":"%s","f":"%s","g":"%s","s1r":"%s","s1c":"%s","s2r":"%s","s2c":"%s"}'

class CodePicnicClient:
	def __init__(self, window):
		global lastRow, lastCol, last_viewport_x, last_viewport_y
		self.window 											= window
		view 															= self.window.active_view()  # Sets initial Post & Col
		lastRow, lastCol 									= view.rowcol(view.sel()[0].a)
		last_viewport_x, last_viewport_y 	= view.viewport_position()
		self.api_url_base 								= settings.get("code_picnic_api_url_base") # "127.0.0.1:4567"
		self.api_endpoint 								= settings.get("code_picnic_api_endpoint") # "/activity"
		self.github_username 							= settings.get("code_picnic_github_username") # "xenda"

	def run(self, edit): 
		global fileHash, lastRow, lastCol

		self.activity 					= []    
		self.threads 						= []
		self.view 							= self.window.active_view()
		position 								= self.view.sel()[0].b
		row, column 						= self.view.rowcol(position)
		viewport_x, viewport_y 	= self.view.viewport_position()

		#region = sublime.Region(position, position)

		# for region in self.view.sel():  
			# if not region.empty():   # If there's something selected, send it
			# 	text = self.view.substr(region)
				
			# 	self.activity.append( self.get_activity_json(self.view, text, position) )
			# 	self.positionst_activity_to_server()
			# else:
			#self.view.window().run_command("select_all") # if not, select all the content
			
			#text = self.view.substr(self.view.sel()[0])
		text = self.view.substr(sublime.Region(0, self.view.size()))
		
		if fileHash == "":
			fileHash = hashlib.md5()
			fileHash.update(text.encode('utf-8'))  
			self.activity.append(self.get_activity_json(self.view, text, position))
			self.post_activity_to_server()
		else:
			m = hashlib.md5()
			m.update(text.encode('utf-8'))

			if ((m.digest() != fileHash.digest()) or (row != lastRow) or (column != lastCol)) or (viewport_x != last_viewport_x) or (viewport_y != last_viewport_y):
				self.activity.append(self.get_activity_json(self.view, text, position))
				self.post_activity_to_server()
				fileHash 	= m
				lastRow 	= row
				lastCol 	= column

		sublime.status_message('Streaming to CodePicnic')
		#self.view.sel().clear() 
		#self.view.sel().add(region)

	def handle_threads(self, threads, offset=0, i=0, dir=1):
		next_threads = []
		
		for thread in self.threads:
			if thread.is_alive():
					next_threads.append(thread)
					continue
			if thread.result == False:
					continue
		
		self.threads = next_threads
		sublime.status_message('Streaming to CodePicnic')

	def get_activity_json(self, view, text, position):
		time_gmt_ms = str(int( time.time() ))
		file_ext = os.path.splitext( view.file_name() )[1] if view.file_name() else ""
		file_size = str( view.size() )    
		file_name_hash = view.file_name().split("/")[-1] if view.file_name() else ""
		row, column = view.rowcol(position)

		selectionStart	= view.sel()[0].a
		selectionEnd		= view.sel()[0].b
		startRow, startColumn = view.rowcol(selectionStart)
		endRow, endColumn = view.rowcol(selectionEnd)
			
		viewport_x, viewport_y = view.viewport_position()
		
		return activity_json_template % (
			time_gmt_ms, 
			row,
			column, 
			viewport_x,
			viewport_y,
			base64.b64encode(text.encode("utf-8")), 
			str(file_name_hash),
			str(self.github_username),
			startRow,
			startColumn,
			endRow,
			endColumn)

	def post_activity_to_server(self):
			if len(self.activity) == 0:
				return
			
			thread = CodePicnicPOSTRequest(self.activity, 5, self.api_url_base, self.api_endpoint) 
			
			self.threads.append(thread)
			
			thread.start()
			
			self.activity = []
			self.handle_threads(self.threads)

class CodePicnicPOSTRequest(threading.Thread):
	def __init__(self, activity_json, timeout, api_url_base, api_endpoint):
		self.activity_json = activity_json
		self.timeout = timeout
		self.api_url_base = api_url_base
		self.api_endpoint = api_endpoint
		self.result = None  
		threading.Thread.__init__(self)

	def run(self):
		try:
			print("[%s] Posting code to %s" % (__name__, self.api_url_base))
			if PY2:
				data = urllib.urlencode({"activity": self.activity_json}, True)
				# data = urllibparse.urlencode()
			else:
				data = urllibparse.urlencode({"activity": self.activity_json}, True)

			headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
			conn = http.client.HTTPConnection(self.api_url_base)
			conn.request("POST", self.api_endpoint, data, headers)
			conn.close()      
			return

		except Exception as e:
			err = '[%s] Error: %s' % (__name__, str(e))  
		print(err)
		self.result = False

class CodePicnicEmitter:
	emitting = False

	def start(self, client):
		self.emitting = True
		self.client = client

		sublime.set_timeout(self.emit, settings.get("code_picnic_interval"))

	def stop(self):
		self.emitting = False
		print("Stopping")

	def emit(self):
		if self.emitting == True:
			self.client.run(True)
			sublime.set_timeout(self.emit, settings.get("code_picnic_interval"))
		else:
			return

code_picnic_emitter = CodePicnicEmitter()

class CodePicnicStartCommand(sublime_plugin.WindowCommand):
	def run(self):
		client = CodePicnicClient(self.window)
		code_picnic_emitter.start(client)

class CodePicnicStopCommand(sublime_plugin.WindowCommand):
	def run(self):
		code_picnic_emitter.stop()