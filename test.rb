

ArsTechnica
RegisterLog in
Home
Main Menu
My Stories: 25
Forums
Subscribe
Jobs
TECHNOLOGY LAB / INFORMATION TECHNOLOGY
How can you make sprint planning fun?
When meetings drag on for hours, developers check out.

by Stack Exchange - Oct 26 2013, 11:13am PET
DEVELOPMENT
 
Stack Exchange
This Q&A is part of a weekly series of posts highlighting common questions encountered by technophiles and answered by users at Stack Exchange, a free, community-powered network of 100+ Q&A sites.
Jacob Spire asked:

Not only are our sprint planning meetings not fun, they're downright dreadful.

The meetings are tedious and boring and take forever (a day, but it feels like a lot longer).

The developers complain about it, and dread upcoming plannings.

Our routine is pretty standard (user story inserted into sprint backlog by priority >> story is taken apart to tasks >> tasks are estimated in hours >> repeat), and I can't figure out what we're doing wrong.

Here are some more details, in response to requests for more information:

Why are the backlog items not inserted and prioritized before sprint kickoff?

User stories are indeed prioritized; we have no idea how long they'll take until we break them down into tasks! From the (excellent) answers here, I see that maybe we shouldn't estimate tasks at all, only the user stories. The reason we estimate tasks (and not stories) is because we've been getting story-estimates terribly wrong—but I guess that's the subject for an altogether different question.

Why are developers complaining?

Meetings are long.
Meetings are monotonous. Story after story, task after task, struggling (yes, struggling) to estimate how long it will take and what it involves.
Estimating tasks makes user-story-estimation seem pointless.
The longer the meeting, the less focus in the room. The less focused colleagues are, the longer the meeting takes. A recursive hate-spiral develops. We've considered splitting the meeting into two days in order to keep people focused, but the developers wouldn't hear of it. One day of planning is bad enough; now we'll have *two*?!
Part of our problem is that we go into very small detail (in order to get more accurate estimations). But when we estimate roughly, we go way off the mark!

To sum up the question:

What are we doing wrong?
What additional ways are there to make the meeting generally more enjoyable?
See the original question here.

Efficiency and companionship

Telastyn answers (27 votes):

Why are the backlog items not inserted and prioritized before sprint kickoff? Wasting developers' time is not fun. Let your team leads work with the product owner and project manager a few days beforehand to prioritize stuff. This goes for planning who is on each sprint team too.
Why is it taking a day to break things out into tasks? If you have a reasonably sized team (2-4 developers, .5-1.5 QA people per developer, 1-2 misc) then you should have 2-4 user stories this sprint. Spend 30 minutes or so with the product owner clarifying requirements, then 30 minutes or so breaking it out to ~8 hour tasks. Don't enter the tasks during the meeting. Just agree as a team what the tasks are, who is responsible for them, and about how long they should take. Agree that "how long they should take (including testing)" fits comfortably within the sprint.
If it's not just breaking things into tasks, what else are you doing? Sure, retrospectives can take 30-60 minutes, but will be shorter as the teams get into a groove.
So in summary—quit wasting people's time and they'll dread the meetings a bit less.

Beyond that, fun and comradarie in the team isn't something you can address in meetings. Go to lunch together, joke around, mix people up to have better personality fits, have mustache growing contests... once the morale is up then people will naturally make sprint planning meetings more lighthearted.

Related: "Sprint item takes longer than expected to be completed. What should we do?"

Less planning, better planning

Sklivvz♦ answers (4 votes):

Your planning sessions are way too long!

Based on my experience, a sprint planning meeting should take no more than 2 hours per week being planned (e.g. a 2 week sprint should take 1/2 day at most), but successful ones should be shorter than that (half of it).

In your particular case: why are you estimating tasks? You should estimate only stories during planning. Tasks can be estimated later by the specific task owners.

A way that worked for me:

Quick intro to the sprint by the PO
Estimation of the sprint capacity
Stories run down and planning poker (timeboxed at 5/10 minutes per story) until there's enough estimated stuff to cover the sprint
Official commitment/forecast by the team
Then, in parallel/pairs/self organized at our desks, tasking and task estimation.

Meeting-style musical chairs

Karl Bielefeldt answers (7 votes):

Planning is one area of scrum where teams have a lot of flexibility. Try something new every sprint until you hit on something that works for your team.

Some successful ideas I've either tried personally, or heard about from other teams:

Do user story creation and prioritization without the entire team. The product owner and/or scrum master can handle a lot of the busy work and just let the team tweak it.
Make your backlog significantly longer than a single sprint. It can take a while to build it up, but if your backlog is long enough, planning meetings are reduced to making small tweaks or addressing recent business developments.
Have estimation meetings separate from sprint planning. If people think the meetings are too long, there's no reason not to split them up.
Specifically plan breaks into the agenda. This is useful if you're often wasting time waiting for one or two team members to return.
Break in the middle of the meeting and assign everyone to task out one or two user stories, then meet back together to report and gain consensus.
Make sure your planning meeting is about what to do, not how to do it. Engineers fall very easily into the latter. If you need to, set up separate design meetings where you discuss the how.
Separate your stories into investigation and implementation. Planning meetings often go too long when team members know too little about what they will be working on, and try to figure it out during the meeting. For example, say you needed to integrate with an API that your team has no experience with. Rather than try to create estimates and tasks during the planning meeting about something you are clueless about, make one investigation story to learn the API, do a simple "hello world" app, and teach it to the team. Then you'll be equipped to plan the actual work.
Keep track during your meetings of specific issues. Not just "planning is boring," but a level of detail like, "we spend a lot of time talking about unclear requirements, and no one seems to know the right answer." Then discuss those specific issues in your retrospectives and brainstorm for specific solutions. Break your problem down until the pieces are easy to solve.
We hold our sprint planning and retrospective at the same time, and are almost always done in 90 minutes, but we are one of the faster teams. We do a big company-wide, longer-term planning meeting every 5 sprints that takes 4-6 hours. Every team is different, of course, but if you're spending an entire day every sprint, there's a lot of room for improvement.

Find more answers or leave your own at the original post. See more Q&A like this at Programmers, a site for conceptual programming questions at Stack Exchange. And of course, feel free to login and ask your own question.

READER COMMENTS 
← OLDER STORY
YOU MAY ALSO LIKE

TOP FEATURE STORY

FEATURE STORY (3 PAGES)
Retina, round two: Apple’s 15-inch 2013 Retina MacBook Pro reviewed
Graphics performance takes a step back even as everything else strides forward.
WATCH ARS VIDEO

iPad hands-on
The new tablets fix the iPad's biggest problems, if you're OK with what they cost.
STAY IN THE KNOW WITH
LATEST NEWS

24 pages of Mavericks review: Ars readers react
MAKE ROOM NEXT TO THE ET CARTRIDGES

DOA: The Galaxy Gear reportedly has a 30 percent return rate at Best Buy
MUCHO MOOLA

FBI seizes over $27 million in bitcoins, likely from Silk Road suspect
JUST IN TIME FOR KITKAT

Android 4.3 schedule for Galaxy S 4, S 4 Active, S III, and Note 2 leaks
SO I SAYS, KISS MY CAPTCHA

reCAPTCHAs are finally readable by normal humans

Review: Acer and Haswell give Chrome OS the battery life it deserves


 
SITE LINKS
About Us
Advertise with us
Contact Us
Reprints
SUBSCRIPTIONS
Subscribe to Ars
MORE READING
RSS Feeds
Newsletters
CONDE NAST SITES
Reddit
Wired
Vanity Fair
Style
Details


VIEW MOBILE SITE
© 2013 Condé Nast. All rights reserved
Use of this Site constitutes acceptance of our User Agreement (effective 3/21/12) and Privacy Policy (effective 3/21/12), and Ars Technica Addendum (effective 5/17/2012)
Your California Privacy Rights
The material on this site may not be reproduced, distributed, transmitted, cached or otherwise used, except with the prior written permission of Condé Nast.

Ad Choices
